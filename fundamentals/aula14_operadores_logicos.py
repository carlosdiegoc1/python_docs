saldo = 100
limite = 150
saque = 200

print(saque > saldo and saque > limite ) # True
print(limite > saldo or saque < limite) # True
print(not saque > saldo) # False

contatos = [] # Uma lista vazia retorna false. 
print(not contatos) # True

nome = "Paulo" # Essa string tem valor, então é verdadeira.
print(not nome) # False

sobrenome = "" # Essa string não tem valor, então é falsa.
print(not sobrenome) # True

# Também podemos utilizar os parenteses para indicar a precedencia das operações.