nome = "Carlos Diego Ferreira Costa"

print("Nome maiusculo: " + nome.upper()) 
print("Nome minusculo: " + nome.lower())
print("Primeira letra maiuscula: " + nome.title()) # Semelhante ao capitalize


nome_com_espaco = "  Carlos  "
print("Nome sem espaços: " + nome_com_espaco.strip()) # Semelhante ao trim
print("Nome sem espaços a esquerda: " + nome_com_espaco.lstrip())
print("Nome sem espaços a direita: " + nome_com_espaco.rstrip())

curso = "Python"
print(curso.center(15, "#"))
print(".".join(curso))