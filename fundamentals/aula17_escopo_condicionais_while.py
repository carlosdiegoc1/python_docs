saldo = 500
cond = ...

def sacar(valor):
    global saldo  
    if(valor <= saldo):
        saldo -= valor
        print(f"Saque realizado com sucesso, seu saldo é: {saldo}")
    else:
        print(f"Não é possível sacar R${valor} pois seu saldo é: R${saldo}")

def verificar_saldo() -> bool:
    global saldo
    return True if saldo > 0  else False #Estrutura ternária!

while(cond != "n"):
    if(verificar_saldo() is False):
       break
    sacar(float(input("Digite o valor que deseja sacar: R$ "))) 
    if(verificar_saldo() is True):
        cond = input("Deseja realizar outro saque? Digite 's' para sim e 'n' para não: ")
    else:
        print("Obrigado por utilizar nosso Auto Banking, mas você não tem nenhuma saldo disponível no momento!")
else:
    print("Obrigado por utilizar nosso Auto Banking!")

# Este código ressalta o papel da indentação na linguagem python pois 
# é a indentação que delimita o escopo de um bloco de código. 
# Além disso demonstra os operadores condicionais e o while.
# Caso queira utilizar o else if, a palavra reservada é "ELIF".
