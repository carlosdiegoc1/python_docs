def hello_world():
    print("Hello world!")
    return None # Caso não seja explicitado, esse será o padrão.

def hello_you(name):
    print(f'Hello {name}, how are you?')

def hello_anonymous(name='Anonymous'): # Valor padrão para o argumento.
    print(f'Hello {name}, who are you?')

hello_world()
hello_you('Carlos')
hello_anonymous()

def sum_numbers(a, b):
    return a + b

print(sum_numbers(10, 20))


def save_car(model, year, identification):
    print(f'Car saved: {model} - {year} - {identification}')

# Formas de passar os argumentos:
save_car('Palio', 2023, 'PDF1899')
save_car(model='Fox', year=2017, identification='PDJ1577')
save_car(**{'model': 'Gol', 'year': 2022, 'identification': 'PDD0770'})
