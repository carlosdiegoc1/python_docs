frutas = ['pêra', 'uva', 'maçã'] # Declaração de lista utilizando colchetes
print(frutas) # Retorna a lista
print(frutas[0]) # Retorna o valor do índice [n] da lista
print(frutas[-1]) # Retorna o valor do último índice da lista

verduras = [] # Declaração de lista vazia

letras = list("string") # Instancia uma lista a partir de um construtor que recebe
#um argumento iterável, nesse caso uma string satifaz essa condição.
print(letras)
print(letras[2:]) # Vai da posição n - 1 até o final da lista
print(letras[:2]) # Vai do começo da lista até a posição n -1
print(letras[0:2]) # Vai da posição 0 até a posição n - 1
print(letras[0:4:2]) # Vai da posição 0 até a posição n - 1 pulando de dois em dois (step)
print(letras[::]) # Percore toda a lista (retorna uma cópia)
print(letras[::-1]) # Inverte a lista

numeros1 = list(range(10)) # Instancia uma lista a partir de um construtor que recebe
#um argumento iterável, nesse caso a função range() satifaz essa condição.
print(numeros1)

# Abaixo podemos ver que a lista é uma coleção de objetos que podem assumir vários tipos
carro = ["Volkswagen", "Fox 1.0", 2023, 42590.99, "PDJ1587"]
for atributo in carro:
    print(atributo)

carros = ["Polo", "Gol", "Fusca", "Creta"]
for indice in enumerate(carros):
    print(f'{indice}')

matriz = [
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1]
]
print(matriz)
print(matriz[0])
print(matriz[0][0])
print(matriz[1][-2])


numeros = [1, 7, 12, 33, 24, 57, 65, 76, 36]
pares = []
for number in numeros:
    if(number % 2 == 0):
        pares.append(number) # Adiciona apenas os números pares na lista pares
print(pares)

# Executa a mesma tarefa anterior em uma única linha [Retorno, Iteração, Condição]
pares = [numero for numero in numeros if numero % 2 == 0]
print(pares)

quadrados = [number ** 2 for number in numeros]
print(quadrados)

consoantes = ['b', 'c', 'd', 'f']
vogais = ['a', 'e', 'i']
print(consoantes)
print(vogais)
consoantes.extend(vogais) # Anexa os objetos de vogais em consoantes (Retorna None)
print(f"Junção da lista consoantes com a lista vogais: {consoantes}")

print(f'O d ocorre no índice: {consoantes.index("d")}') # Retorna apenas a posição da primeira aparição do objeto.

print(f"Contagem do número de vezes que o elemento 12 aparece na lista pares: {pares.count(12)}")

print(f"Cópia de lista pares, porém é um objetivo diferente: {pares.copy()}")

pares.clear() # Limpa a lista pares (Retorna None)
print(f"Limpa a lista pares: {pares}")


linguagens = ['python', 'js', 'c#', "c++"]
print(linguagens)

linguagens.reverse()
print(linguagens)

linguagens.pop() # Remove o último elemento da lista
print(linguagens)

linguagens.pop(0) # Remove o primeiro elemento da lista
print(linguagens)

linguagens.remove('js') # Remove apenas a primeira ocorrência de js.
print(linguagens)

pessoas = ['carlos', 'flavia', 'caio', 'eduarda', 'maria']
print(pessoas)

pessoas.sort()
print(pessoas)

sorted(pessoas)
print(pessoas)

pessoas.sort(reverse=True)
print(pessoas)

pessoas.sort(key=lambda x: len(x)) # Caso dois elementos tenham o mesmo tamanho, o que aparece primeiro irá aparecer antes na ordem.
print(pessoas)

pessoas.sort(key=lambda x: len(x), reverse=True)
print(pessoas)

print(f"Tamanho da lista de pessoas: {len(pessoas)}")





