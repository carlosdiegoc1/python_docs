pessoa = {"nome": "Guilherme", "idade": 28}
pessoa = dict(nome = "Guilherme", idade = 28)
pessoa['telefone'] = '3341-4224' # {"nome": "Guilherme", "idade": 28, "telefone": "3341-4224"}
print(pessoa)


# Para acessar um valor, seja para ler ou atualizar

print(pessoa['nome'])
pessoa['nome'] = 'Diego'
print(pessoa['nome'])


contatos = {
    "carlos@hotmail.com": {"nome": "Carlos", "idade": 30},
    "flavia@hotmail.com": {"nome": "Flávia", "idade": 26},
    "maria@hotmail.com": {"nome": "Maria", "idade": 58}
} # Dicionário aninhado é bastante útil!

print(contatos['carlos@hotmail.com']['idade'])


for chave, valor in contatos.items():
    print(chave, valor)
