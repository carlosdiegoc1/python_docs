
#IMPORTANTE! O conjunto não garante a ordem dos elementos!

conjunto = set([1, 2, 3, 1, 4, 5, 3, 16]) #Conjunto a partir de uma lista.
print(conjunto)

palavra = "abacaxi"
conjunto_palavra = set(palavra) #Conjunto a partir de um objeto iterável (string).
print(conjunto_palavra)

carros = ("palio", "gol", "monza", "fox", "palio") #Conjunto a partir de uma tupla.
conjunto_carros = set(carros)
print(conjunto_carros)

linguagens = {"python", "java", "python"} #Forma de instanciar um conjunto.
print(linguagens)

#Para acessar os valores é preciso converter o conjunto para uma lista.

numeros = {1, 2, 3, 6, 7, 10, 2, 1, 5, 6}
numeros = list(numeros)
print(numeros[0])

for indice, carro in enumerate(carros): # É possível percorrer e também usar o enumerate.
    print(f'{indice}: {carro}')

conjunto_a = {1, 2, 3}
conjunto_b = {2, 3, 4}

conjunto_ab = conjunto_a.union(conjunto_b)
print(conjunto_ab)

conjunto_aeb = conjunto_a.intersection(conjunto_b)
print(conjunto_aeb)

conjunto_amenosb = conjunto_a.difference(conjunto_b)
print(conjunto_amenosb)

conjunto_asimetricob = conjunto_a.symmetric_difference(conjunto_b) # É a diferença anterior, partindo de ambos os lados.
print(conjunto_asimetricob)

conjunto_c = {'a', 'b', 'c', 'd', 'e'}
conjunto_d = {'a', 'e'}

print(conjunto_d.issubset(conjunto_c)) #Todos os elementos de d estão em c.
print(conjunto_c.issuperset(conjunto_d)) #C contém todos os elementos de d

conjunto_e = {1, 2, 3, 4}
conjunto_f = {5, 6, 7}
print(conjunto_e.isdisjoint(conjunto_f)) #Nenhum elemento de e é também elemento de f.

numeros.remove(10) #Resulta em erro caso o elemento não exista no conjunto.
numeros.pop()
len(numeros)
1 in numeros
70 in numeros



