import openpyxl as xl
from openpyxl.styles import Font, Alignment, Border, Side
from openpyxl.utils import get_column_letter

# Abrindo planilha excel existente.
wb = xl.load_workbook('C:\\workspaces\\python_docs\\automating_spreadsheets\\transactions.xlsx')
sheet = wb['Planilha1']

# Variáveis globais
thin = Side(border_style="thin", color="000000")

#Auto-ajuste da largura das colunas.
for column in range(1, sheet.max_column + 3):
    column_letter = get_column_letter(column)
    target_column = sheet.column_dimensions[column_letter]
    column_length = target_column.width
    for row in range(1, sheet.max_row + 1):
        cell = sheet[f'{column_letter}{row}']
        cell_length = len(str(cell.value))      
        try:
            if cell_length > column_length:
                column_length = cell_length
        except TypeError:
            pass
    sheet.column_dimensions[column_letter].width = column_length

# Criação da coluna com os novos preços.
new_price_collum = sheet.cell(1, 5)
new_price_collum.value = 'new_price'
new_price_collum.font = Font(bold=True)
new_price_collum.alignment = Alignment(horizontal='center')
new_price_collum.border = Border(top=thin, left=thin, right=thin, bottom=thin)

for row in range(2, sheet.max_row + 1):
    cell = sheet.cell(row, 3)   
    corrected_price = cell.value * 0.9
    corrected_price_cell = sheet.cell(row, 5)
    corrected_price_cell.value = corrected_price
    corrected_price_cell.number_format = 'R$ 0.00'
    corrected_price_cell.alignment = Alignment(horizontal='center')
    corrected_price_cell.font = Font(color="FF0000")
    corrected_price_cell.border = Border(top=thin, left=thin, right=thin, bottom=thin)

# Criação da coluna com as novas datas.
new_date_collum = sheet.cell(1,6)
new_date_collum.value = 'new_date'
new_date_collum.font = Font(bold=True)
new_date_collum.alignment = Alignment(horizontal='center')
new_date_collum.border = Border(top=thin, left=thin, right=thin, bottom=thin)

for row in range(2, sheet.max_row + 1):
    cell = sheet.cell(row, 4)   
    corrected_date_cell = sheet.cell(row, 6)
    corrected_date_cell.value = cell.value
    corrected_date_cell.number_format = 'yyyy-mm-dd'
    corrected_date_cell.alignment = Alignment(horizontal='center')
    corrected_date_cell.font = Font(color="FF0000")
    corrected_date_cell.border = Border(top=thin, left=thin, right=thin, bottom=thin)

# Salvando planilha em um novo arquivo.
wb.save('C:\\workspaces\\python_docs\\automating_spreadsheets\\transactions_new.xlsx') 
print("Successfully processed!")



